//% color="#17959d" iconWidth=50 iconHeight=40
namespace BadgeDisplay {
    //% block="BadgeDisplay initialize serial RX:[RX] TX:[TX]" blockType="command"
    //% RX.shadow="dropdown" RX.options="RX"
    //% TX.shadow="dropdown" TX.options="TX"
    export function BadgeDisplay_Init(parameter: any, block: any) {
        let rx = parameter.RX.code;
        let tx = parameter.TX.code;
        Generator.addInclude("addIncludeHuizhang","#include <Huizhang.h>");
        Generator.addObject("addHuiZhang","HuiZhang",`hz(${rx}, ${tx});`)
    }

    //% block="BadgeDisplay Clear Screen" blockType="command"
    //% PIN.shadow="dropdown" PIN.options="PIN"
    export function BadgeDisplay_Clear_Screen(parameter: any, block: any) {
        // let pin = parameter.PIN.code;
        Generator.addInclude("addIncludeHuizhang","#include <Huizhang.h>");
        // Generator.addObject("addHuiZhang","HuiZhang",`hz_${pin}(${pin});`);
        Generator.addCode(`hz.SendClr();`)
    }

    //% block="BadgeDisplay display Number[NUM]" blockType="command"
    //% PIN.shadow="dropdown" PIN.options="PIN"
    //% NUM.shadow="number" NUM.defl=123
    export function displayNum(parameter: any, block: any) {
        // let pin = parameter.PIN.code;
        let num = parameter.NUM.code;
        Generator.addInclude("addIncludeHuizhang","#include <Huizhang.h>");
        // Generator.addObject("addHuiZhang","HuiZhang",`hz_${pin}(${pin});`);
        // Generator.addObject(`displayNum`, `void`, `displayNum_${pin}(String num) {\n\thz.Send('n');\n\t${pin}.Send(num);\n}`);
        // if (Generator.board !== 'esp32') {
        //     Generator.addSetup(`${pin}.begin`, `${pin}.begin(115200);`);
        // }
        // Generator.addCode(`displayNum_${pin}(String(${num}));`)
        Generator.addCode(`hz.SendNum(String(${num}));`)
    }

    //% block="BadgeDisplay display Number[NUM] at X[X]" blockType="command"
    //% PIN.shadow="dropdown" PIN.options="PIN"
    //% X.shadow="range" X.params.min="1" X.params.max="44" X.defl="1"
    //% NUM.shadow="number" NUM.defl=123
    export function displayNumAtX(parameter: any, block: any) {
        // let pin = parameter.PIN.code;
        let x = parameter.X.code;
        let num = parameter.NUM.code;
        Generator.addInclude("addIncludeHuizhang","#include <Huizhang.h>");
        // Generator.addObject("addHuiZhang","HuiZhang",`hz_${pin}(${pin});`);
        // Generator.addObject(`displayNumAtX`, `void`, `displayNumAtX_${pin}(String num, int x) {\n\thz.Send('n');\n\thz.Send(x);\n\thz.Send('/');\n\t${pin}.Send(num);\n}`);
        // if (Generator.board !== 'esp32') {
        //     Generator.addSetup(`${pin}.begin`, `${pin}.begin(115200);`);
        // }
        Generator.addCode(`hz.SendNumAtX(String(${num}),${x});`)
    }

    //% block="BadgeDisplay display String[STR]" blockType="command"
    //% PIN.shadow="dropdown" PIN.options="PIN"
    //% STR.shadow="string" STR.defl="YES"
    export function displayStr(parameter: any, block: any) {
        // let pin = parameter.PIN.code;
        let str = parameter.STR.code;
        Generator.addInclude("addIncludeHuizhang","#include <Huizhang.h>");
        // Generator.addObject("addHuiZhang","HuiZhang",`hz_${pin}(${pin});`);
        // Generator.addObject(`displayStr`, `void`, `displayStr_${pin}(String str) {\n\thz.Send('s');\n\t${pin}.Send(str);\n}`);
        // if (Generator.board !== 'esp32') {
        //     Generator.addSetup(`${pin}.begin`, `${pin}.begin(115200);`);
        // }
        // Generator.addCode(`displayStr_${pin}(String(${str}));`)
        Generator.addCode(`hz.SendStr(String(${str}));`)
    }

    //% block="BadgeDisplay display String[STR] at X[X]" blockType="command"
    //% PIN.shadow="dropdown" PIN.options="PIN"
    //% X.shadow="range" X.params.min="1" X.params.max="44" X.defl="1"
    //% STR.shadow="string" STR.defl="YES"
    export function displayStrAtX(parameter: any, block: any) {
        // let pin = parameter.PIN.code;
        let x = parameter.X.code;
        let str = parameter.STR.code;
        Generator.addInclude("addIncludeHuizhang","#include <Huizhang.h>");
        // Generator.addObject("addHuiZhang","HuiZhang",`hz_${pin}(${pin});`);
        // Generator.addObject(`displayStrAtX`, `void`, `displayStrAtX_${pin}(String str, int x) {\n\thz.Send('s');\n\thz.Send(x);\n\thz.Send('/');\n\t${pin}.Send(str);\n}`);
        // if (Generator.board !== 'esp32') {
        //     Generator.addSetup(`${pin}.begin`, `${pin}.begin(115200);`);
        // }
        // Generator.addCode(`displayStrAtX_${pin}(String(${str}), ${x});`)
        Generator.addCode(`hz.SendStrAtX(String(${str}),${x});`)
    }

    //% extenralFunc
    export function getBuiltinFunc_() {
        return [
            {"matrix":"0000000000000111011100010001000101000000000110000000001100000000010100000001000100000100000100010000000101000000000100000"},
            {"matrix":"0000000000000111011100011111111101111111111111111111111111111111110111111111000111111100000111110000000111000000000100000"},
            {"matrix":"0000000000000000000000011000001001111000101011110010001011000000000000000000000000000000000111110000000000000000000000000"},
            {"matrix":"0000000000000000000000010100010100010000010000000000000000000000000010000010000010001000000011100000000000000000000000000"},
            {"matrix":"0000000000000000000000000000000000110000011001100000110000000000000001111100000100000100010000000100000000000000000000000"},
            {"matrix":"0000000000000000000000010000000100010000010000010001000001000001000100000001000000000000000011100000001000100000000000000"},
            {"matrix":"0100000010001110000111110000011001100000110001100000110010100001010101000010111100001110010000001000000000000000001110000"},
            {"matrix":"0000000000000000000000110110110111111101111101110001110001000001000000000000000000000000000100010000000111000000000000000"},
            {"matrix":"0000000000000000000000011110111100011000110000110001100001100011000010000010000000000000000011100000001000100000000000000"}
        ]
    }
    //% block="BadgeDisplay Matrix [MT] At X[X]" blockType="command"
    //% MT.shadow="matrix" MT.params.row=11 MT.params.column=11 MT.defl="0000000000000111011100010001000101000000000110000000001100000000010100000001000100000100000100010000000101000000000100000"
    //% X.shadow="range" X.params.min="1" X.params.max="44" X.defl="1"
    //% MT.params.builtinFunc="getBuiltinFunc_"
    export function setMatrix(parameter: any, block: any) {
        let matrix = parameter.MT.code;
        Generator.addInclude("addIncludeHuizhang","#include <Huizhang.h>");
        Generator.addObject("addhz_matrix","uint16_t",`hz_matrix[11];`);
        let hz_matrix:any[] = [0,0,0,0,0,0,0,0,0,0,0];
        for (let i = 0; i < 11; i++) {
            for (let j = 0; j < 11; j++) {
                hz_matrix[i] += matrix.charAt(i + 11 * j) << (10 - j);
            }
            if (hz_matrix[i] < 0x10) {
                hz_matrix[i] = "0x00" + hz_matrix[i].toString(16);
            } else if (hz_matrix[i] < 0x100) {
                hz_matrix[i] = "0x0" + hz_matrix[i].toString(16);
            } else {
                hz_matrix[i] = "0x" + hz_matrix[i].toString(16);
            }
            if (i % 2) Generator.addCode(`hz_matrix[${i-1}] = ${hz_matrix[i-1]};hz_matrix[${i}] = ${hz_matrix[i]};`);
            else if (i == 10) Generator.addCode(`hz_matrix[${i}] = ${hz_matrix[i]};`);
        }
        let x = parameter.X.code;
        Generator.addCode(`hz.SendMatrix(hz_matrix, ${x});`);
    }

    //% block="BadgeDisplay Matrix End" blockType="command"
    //% PIN.shadow="dropdown" PIN.options="PIN"
    export function displayMatrixEnd(parameter: any, block: any) {
        // let pin = parameter.PIN.code;
        Generator.addInclude("addIncludeHuizhang","#include <Huizhang.h>");
        // Generator.addObject("addHuiZhang","HuiZhang",`hz_${pin}(${pin});`);
        // Generator.addCode(`${pin}.Send();`)
        Generator.addCode(`hz.SendMatrixDisplay();`)
    }
}